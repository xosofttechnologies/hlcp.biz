var ContactPage = function () {

    return {
        
    	//Basic Map
        initMap: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
				scrollwheel: false,
                  lat: 31.47203,
                  lng: 74.37599,
			  });
			  
			  var marker = map.addMarker({
				lat: 31.47203,
				lng: 74.37599,
	            title: 'Company, Inc.'
		       });
			});
        },

        //Panorama Map
        initPanorama: function () {
		    var panorama;
		    $(document).ready(function(){
		      panorama = GMaps.createPanorama({
		        el: '#panorama',
                  lat: 31.47203,
                  lng: 74.37599,
		      });
		    });
		}        

    };
}();