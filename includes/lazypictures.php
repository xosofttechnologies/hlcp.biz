<div class="cbp-item web-design logos">
    <div class="cbp-caption">
        <div class="cbp-caption-defaultWrap">
            <img  src="editedimg/p3.jpg" alt="">
        </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <ul class="link-captions">

                        <li><a href="editedimg/p3.jpg" class="cbp-lightbox" data-title="h.l.c 200"><i
                                    class="rounded-x icon-magnifier-add"></i></a></li>
                    </ul>
                    <div class="cbp-l-grid-agency-title">h.l.c 200</div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="cbp-item life">
    <div class="cbp-caption">
        <div class="cbp-caption-defaultWrap">
            <img  src="editedimg/p4.jpg" alt="">
        </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <ul class="link-captions">

                        <li><a href="editedimg/p4.jpg" class="cbp-lightbox" data-title="life black 200"><i
                                    class="rounded-x icon-magnifier-add"></i></a></li>
                    </ul>
                    <div class="cbp-l-grid-agency-title">life black 200</div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="cbp-item life">
    <div class="cbp-caption">
        <div class="cbp-caption-defaultWrap">
            <img  src="editedimg/p5.jpg" alt="">
        </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <ul class="link-captions">

                        <li><a href="editedimg/p5.jpg" class="cbp-lightbox" data-title="life white 150"><i
                                    class="rounded-x icon-magnifier-add"></i></a></li>
                    </ul>
                    <div class="cbp-l-grid-agency-title">life white 150</div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="cbp-item dht">
    <div class="cbp-caption">
        <div class="cbp-caption-defaultWrap">
            <img  src="editedimg/p6.jpg" alt="">
        </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <ul class="link-captions">

                        <li><a href="editedimg/p6.jpg" class="cbp-lightbox" data-title="dye house trolly"><i
                                    class="rounded-x icon-magnifier-add"></i></a></li>
                    </ul>
                    <div class="cbp-l-grid-agency-title">dye house trolly</div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="cbp-item life roadsafety">
    <div class="cbp-caption">
        <div class="cbp-caption-defaultWrap">
            <img  src="editedimg/p7.jpg" alt="">
        </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <ul class="link-captions">

                        <li><a href="editedimg/p7.jpg" class="cbp-lightbox" data-title="road seperater"><i
                                    class="rounded-x icon-magnifier-add"></i></a></li>
                    </ul>
                    <div class="cbp-l-grid-agency-title">road seperater</div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="cbp-item life">
    <div class="cbp-caption">
        <div class="cbp-caption-defaultWrap">
            <img  src="editedimg/p8.jpg" alt="">
        </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <ul class="link-captions">

                        <li><a href="editedimg/p8.jpg" class="cbp-lightbox" data-title="life white 500"><i
                                    class="rounded-x icon-magnifier-add"></i></a></li>
                    </ul>
                    <div class="cbp-l-grid-agency-title">life white 500</div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="cbp-item primax">
    <div class="cbp-caption">
        <div class="cbp-caption-defaultWrap">
            <img  src="editedimg/p9.jpg" alt="">
        </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <ul class="link-captions">

                        <li><a href="editedimg/p9.jpg" class="cbp-lightbox" data-title="primax blue 1500"><i
                                    class="rounded-x icon-magnifier-add"></i></a></li>
                    </ul>
                    <div class="cbp-l-grid-agency-title">primax blue 1500</div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="cbp-item graphic">
    <div class="cbp-caption">
        <div class="cbp-caption-defaultWrap">
            <img  src="editedimg/p10.jpg" alt="">
        </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <ul class="link-captions">

                        <li><a href="editedimg/p10.jpg" class="cbp-lightbox" data-title="prime black 300"><i
                                    class="rounded-x icon-magnifier-add"></i></a></li>
                    </ul>
                    <div class="cbp-l-grid-agency-title">prime black 300</div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="cbp-item life">
    <div class="cbp-caption">
        <div class="cbp-caption-defaultWrap">
            <img  src="editedimg/p11.jpg" alt="">
        </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <ul class="link-captions">

                        <li><a href="editedimg/p11.jpg" class="cbp-lightbox" data-title="life white 50"><i
                                    class="rounded-x icon-magnifier-add"></i></a></li>
                    </ul>
                    <div class="cbp-l-grid-agency-title">life white 50</div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="cbp-item graphic logos">
    <div class="cbp-caption">
        <div class="cbp-caption-defaultWrap">
            <img  src="editedimg/p2.jpg" alt="">
        </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <ul class="link-captions">

                        <li><a href="editedimg/p2.jpg" class="cbp-lightbox" data-title="h.l.c blue 500"><i
                                    class="rounded-x icon-magnifier-add"></i></a></li>
                    </ul>
                    <div class="cbp-l-grid-agency-title">H.L.C blue 500</div>

                </div>
            </div>
        </div>
    </div>
</div>