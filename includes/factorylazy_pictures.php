<ul class="list-unstyled row portfolio-box no-margin">

    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="editedimg/smalpixels/1.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/1.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery" href="editedimg/smalpixels/2.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/2.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="editedimg/smalpixels/3.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/3.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>
    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="editedimg/smalpixels/4.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/4.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="editedimg/smalpixels/5.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/5.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="editedimg/smalpixels/6.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/6.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>
    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="editedimg/smalpixels/7.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/7.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="editedimg/smalpixels/8.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/8.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="editedimg/smalpixels/9.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/9.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>
    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="editedimg/smalpixels/10.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/10.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="editedimg/smalpixels/11.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/11.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4">
        <a class="thumbnail fancybox" data-rel="gallery"  href="editedimg/smalpixels/12.jpg">
            <img class="full-width img-responsive" src="editedimg/smalpixels/12.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>
    </li>
</ul>