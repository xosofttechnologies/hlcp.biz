<ul class="row block-grid-v2">
    <li class="col-md-4 col-sm-6 md-margin-bottom-30">
        <div class="easy-block-v1">
            <img class="img-responsive" src="editedimg/p1.jpg" alt="">

        </div>
        <div class="block-grid-v2-info rounded-bottom">
            <h3><a href="#">Primax product</a></h3>
            <p>Primax tanks are made with high quality plastic.The ideal small tank for fresh drinking water.</p>

        </div>
    </li>
    <li class="col-md-4 col-sm-6 md-margin-bottom-30">
        <div class="easy-block-v1">
            <img class="img-responsive" src="editedimg/p2.jpg" alt="">

        </div>
        <div class="block-grid-v2-info rounded-bottom">
            <h3><a href="#">H.l.c blue tank </a></h3>
            <p>H.L.C tanks for fresh drinking water.The most popular size to meet local government building requirements.</p>
        </div>
    </li>
    <li class="col-md-4 col-sm-12">
        <div class="easy-block-v1">
            <img class="img-responsive" src="editedimg/p4.jpg" alt="">

        </div>
        <div class="block-grid-v2-info rounded-bottom">
            <h3><a href="#">Life black tank</a></h3>
            <p>Do you need water supply for your home? This water tank is ideal, and conveniently tucks away in a corner..</p>
        </div>
    </li>
</ul>