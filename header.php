<?php $activePage = basename($_SERVER['PHP_SELF'], ".php"); ?>
<div class="header">
    <div class="container">
        <!-- Logo -->
        <a class="logo" href="/">
            <img src="logo1-default.png" alt="Logo">
        </a>
        <!-- End Logo -->

        <!-- Topbar -->
        <div class="topbar">
            <ul class="loginbar pull-right">

            </ul>
        </div>
        <!-- End Topbar -->

        <!-- Toggle get grouped for better mobile display -->
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target=".navbar-responsive-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars"></span>
        </button>
        <!-- End Toggle -->
    </div><!--/end container-->

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
        <div class="container">
            <ul class="nav navbar-nav">
                <!-- Home -->
                <li class=" <?= ($activePage == 'index') ? 'active' : ''; ?> ">
                    <a href="/">
                        Home
                    </a>

                </li>
                <li class=" <?= ($activePage == 'product') ? 'active' : ''; ?> ">
                    <a href="products">
                     Products
                    </a>

                </li>
                <li class=" <?= ($activePage == 'factorytour') ? 'active' : ''; ?> ">
                    <a href="factorytour">
                        Factory tour
                    </a>

                </li>
                <li class="<?= ($activePage == 'aboutus') ? 'active' : ''; ?> ">
                    <a href="aboutus">
                        About us
                    </a>

                </li>
                <li class="<?= ($activePage == 'contactus') ? 'active' : ''; ?> ">
                    <a href="contactus">
                        Contact us
                    </a>

                </li>
                <!-- End Home -->

                <!-- Search Block -->
                <!-- End Search Block -->
            </ul>
        </div><!--/end container-->
    </div><!--/navbar-collapse-->
</div>
<!--<li class="dropdown <?/*= ($activePage == 'contactus') ? 'active bluetag' : ''; */?> ">
    <a href="contactus.php">
        <p class="dropdown <?/*= ($activePage == 'contactus') ? 'anchortag' : 'x'; */?> ">Contact Us</p>
    </a>

</li>-->