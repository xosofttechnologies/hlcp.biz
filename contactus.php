<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <title>Contact</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="hlcp.png">
    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css'
          href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms-ie8.css"><![endif]-->
    <link rel="stylesheet" href="assets/plugins/parallax-slider/css/parallax-slider.css">
    <link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">


    <!-- CSS Page Style -->

    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/theme-skins/dark.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">
    <style>
        .hidedata{
            display: none;
        }
        .showdata{
          display: block;
        }
        .redcolor{
            color:red;

        }
        .buttonload {
            background-color: #4CAF50; /* Green background */
            border: none; /* Remove borders */
            color: white; /* White text */
            padding: 12px 16px; /* Some padding */
            font-size: 16px /* Set a font size */
        }

    </style>
</head>

<body class="dark">

<div class="wrapper">
    <!--=== Header ===-->
    <?php include('header.php'); ?>

    <div class="breadcrumbs">
        <div class="container">
            <h1 class="text-center">Our Contacts</h1>

        </div>
    </div>
    <div id="map" class="map">
    </div><!---/map-->

    <div class="container content">
        <div class="row margin-bottom-30">
            <div class="col-md-9 mb-margin-bottom-30">
                <div class="headline"><h2>Contact Form</h2></div>
                <!--id="form" onsubmit="return false"-->
                <div class="alert alert-success hidedata" style="background-color: #a8daa8;" id="emailsent"role="alert">
                    Email has been sent.
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" style="color: red;">&times;</a>
                </div>
                <form id="sky-form3" class="sky-form contact-style">
                    <fieldset class="no-padding">
                        <label>Name <span class="color-red">*</span></label>
                        <div class="row sky-space-20">
                            <div class="col-md-7 col-md-offset-0">
                                <div>
                                    <input type="text" name="name" id="name" class="form-control" required>
                                </div>
                            </div>
                        </div>

                        <label>Email <span class="color-red">*</span></label>
                        <div class="row sky-space-20">
                            <div class="col-md-7 col-md-offset-0">
                                <div>
                                    <input type="text" name="email" id="email" class="form-control" required>
                                </div>
                            </div>
                        </div>

                        <label>Phone <span class="color-red">*</span></label>
                        <div class="row sky-space-20">
                            <div class="col-md-7 col-md-offset-0">
                                <div>
                                    <input type="text" name="phone" id="phone" class="form-control" required>
                                </div>
                            </div>
                        </div>

                        <label>Message <span class="color-red">*</span></label>
                        <div class="row sky-space-20">
                            <div class="col-md-11 col-md-offset-0">
                                <div>
                                    <textarea rows="8" name="message" id="message" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>

                        <p>
                            <button type="submit" style="margin-top: 2%" class="btn btn-u" id="form-submit" ><i class="" id="spin"></i> Send
                                Message
                            </button>
                        </p>
                    </fieldset>


                </form>

            </div><!--/col-md-9-->

            <div class="col-md-3">
                <!-- Contacts -->
                <div class="headline"><h2>Contacts</h2></div>
                <ul class="list-unstyled who margin-bottom-30">
                    <li><a href="#"><i class="fa fa-home"></i>5B Streat, City 50987 New Town US</a></li>
                    <li><a href="#"><i class="fa fa-envelope"></i>info@example.com</a></li>
                    <li><a href="#"><i class="fa fa-phone"></i>1(222) 5x86 x97x</a></li>
                    <li><a href="#"><i class="fa fa-globe"></i>http://www.example.com</a></li>
                </ul>

                <!-- Business Hours -->
                <div class="headline"><h2>Business Hours</h2></div>
                <ul class="list-unstyled margin-bottom-30">
                    <li><strong>Monday-Friday:</strong> 10am to 8pm</li>
                    <li><strong>Saturday:</strong> 11am to 3pm</li>
                    <li><strong>Sunday:</strong> Closed</li>
                </ul>


            </div><!--/col-md-3-->
        </div><!--/row-->

        <!-- Owl Clients v1 -->

        <!-- End Owl Clients v1 -->
    </div>

    <div class="footer-v1">

        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            H.L.C.P 2018 &copy; All Rights Reserved.
                            <a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>
                        </p>
                    </div>

                    <!-- Social Links -->
                    <div class="col-md-6">
                        <ul class="footer-socials list-inline">
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-original-title="Skype">
                                    <i class="fa fa-skype"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-original-title="Google Plus">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-original-title="Linkedin">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-original-title="Pinterest">
                                    <i class="fa fa-pinterest"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-original-title="Twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-original-title="Dribbble">
                                    <i class="fa fa-dribbble"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Social Links -->
                </div>
            </div>
        </div><!--/copyright-->
    </div>
    <!--=== End Footer Version 1 ===-->
</div><!--/End Wrapepr-->
<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAm4-5Agqji5Tu6isdh-6Snn6dR8Yi-lFw"></script>
<script type="text/javascript" src="assets/plugins/gmap/gmap.js"></script>
<script type="text/javascript" src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js"></script>
<script type="text/javascript" src="assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/forms/login.js"></script>
<script type="text/javascript" src="assets/js/forms/contact.js"></script>
<script type="text/javascript" src="assets/js/pages/page_contacts.js"></script>

<script type="text/javascript" src="assets/js/plugins/owl-carousel.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init();
        ContactPage.initMap();
        LoginForm.initLoginForm();
        ContactForm.initContactForm();
        OwlCarousel.initOwlCarousel();

    });


     $('#form-submit').click(function(e) {
            e.preventDefault();

            if( $('form[id="sky-form3"]').valid())
            {
                $('#spin').addClass('fa fa-spinner fa-spin');
                $('#emailsent').removeClass('showdata').addClass('hidedata');
                $('#form-submit').attr("disabled", "disabled");
                $.ajax({
                    url: "sendemail.php",
                    type: "POST",
                    data: {
                        "name": $("input[name=name]").val(),
                        "email": $("input[name=email]").val(),
                        "phone": $("input[name=phone]").val(),
                        "message": $("textarea[name=message]").val()
                    },
                    success: function (response)
                    {
                        $('#emailsent').removeClass('hidedata').addClass('showdata');
                        /*setTimeout(function() {
                            $('#emailsent').removeClass('showdata').addClass('hidedata');
                        }, 4000);*/
                        $('#spin').removeClass('fa fa-spinner fa-spin');
                        $('#form-submit').removeAttr("disabled");

                        $("input[name=name]").val('');
                        $("input[name=email]").val('');
                       $("input[name=phone]").val('');
                       $("textarea[name=message]").val('');
                    }
                });
            }
            else
            {
                $('#emailsent').removeClass('showdata').addClass('hidedata');
            }

        });

  /*  var name = $('#name').val();
    var phone = $('#phone').val();
    var email = $('#email').val();
    var message = $('#message').val();

    $('form[id="form"]').validate({
        errorClass: 'redcolor',
        rules: {
            name: 'required',
            phone: 'required',
            email: {
                required: true,
                email: true,
            },
            message: {
                required: true,

            }
        },
        messages: {
            name: 'Name is required',
            email: 'Email is required',
            phone: 'phone is required',
            message: 'message is required'
        },
        submitHandler: function (form) {
            form.submit();

        }
    });
*/
    /* $('#form-submit').click(function () {

         $val=0;
         $checktrue=0;
         if(!$("input[name=name]").val())
         {
             $val=1;
             $checktrue++;
             $('#name_v').removeClass('hidedata').addClass('showdata');
         }
         else
         {
             $checktrue--;
             $('#name_v').removeClass('showdata').addClass('hidedata');
         }
         if(!$("input[name=email]").val())
         {
             $val=1;
             $checktrue++;
             $('#email_v').removeClass('hidedata').addClass('showdata');
         }
         else
         {
             $checktrue--;
             $('#email_v').removeClass('showdata').addClass('hidedata');
         }
         if(!$("input[name=phone]").val())
         {
             $val=1;
             $checktrue++;
             $('#phone_v').removeClass('hidedata').addClass('showdata');
         }
         else
         {
             $checktrue--;
             $('#phone_v').removeClass('showdata').addClass('hidedata');
         }
         if(!$("textarea[name=message]").val())
         {
             $val=1;
             $checktrue++;
             $('#mesg_v').removeClass('hidedata').addClass('showdata');
         }
         else
         {
             $checktrue--;
             $('#mesg_v').removeClass('showdata').addClass('hidedata');
         }
         if($checktrue==0 || $checktrue==-4)
         {
             $('#spin').addClass('fa fa-spinner fa-spin');
             $('#emailsent').removeClass('showdata').addClass('hidedata');
             $(this).attr("disabled", "disabled");
             $.ajax({
                 url: "sendemail.php",
                 type: "POST",
                 data: {
                     "name": $("input[name=name]").val(),
                     "email": $("input[name=email]").val(),
                     "phone": $("input[name=phone]").val(),
                     "message": $("textarea[name=message]").val()
                 },
                 success: function (response)
                 {
                     $('#emailsent').removeClass('hidedata').addClass('showdata');
                     setTimeout(function() {
                         $('#emailsent').removeClass('showdata').addClass('hidedata');
                     }, 4000);
                     $('#spin').removeClass('fa fa-spinner fa-spin');
                     $('#form-submit').removeAttr("disabled");
                 }
             });
         }
         else {

             $('#emailsent').removeClass('showdata').addClass('hidedata');

         }

     });*/

</script>
<!--[if lt IE 9]>
<!--<script src="assets/plugins/respond.js"></script>
<script src="assets/plugins/html5shiv.js"></script>
<script src="assets/plugins/placeholder-IE-fixes.js"></script>-->
<![endif]-->

</body>
</html>