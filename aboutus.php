<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <title>About</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="hlcp.png">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css'
          href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
    <!--[if lt IE 9]>
   <!-- <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms-ie8.css">
    <link rel="stylesheet" href="assets/plugins/parallax-slider/css/parallax-slider.css">
    <link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css"> -->


    <!-- CSS Page Style -->

    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/theme-skins/dark.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">
</head>

<body class="dark">

<div class="wrapper">
    <!--=== Header ===-->
<?php include('header.php'); ?>

    <div class="bg-grey content-lg"style="min-height: 86.9vh">
        <div class="container" style="margin-bottom: -6%;">
            <div class="row">
                <div class="col-md-6">
                    <div class="responsive-video margin-bottom-30">
                        <iframe src="https://player.vimeo.com/video/47911018" width="530" height="300" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                    </div>
                </div>
                <div class="col-md-6">
                    <h2 class="title-v2">WATCH ABOUT US</h2>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p><br>


                </div>
            </div>
        </div>
    </div>

 <!--   <div class="container team-v1 content-sm">
        <div class="margin-bottom-40 text-center">
            <h2 class="title-v2 title-center">MEET OUR TEAM</h2>
            <p>We <strong>meet</strong> and get to know you. You tell us and we listen. <br>
                We build your website to realise your vision and we <strong>deliver</strong> the ready product.</p>
        </div>

        <ul class="list-unstyled row">
            <li class="col-sm-3 col-xs-6 md-margin-bottom-30">
                <div class="team-img">
                    <img class="img-responsive" src="owner/7.jpg" alt="">
                </div>
                <h3>John Brown</h3>
                <h4>/ Technical Director</h4>
                <p>Technical Director mi porta gravida at eget metus id elit mi egetine...</p>
            </li>
            <li class="col-sm-3 col-xs-6 md-margin-bottom-30">
                <div class="team-img">
                    <img class="img-responsive" src="owner/11.jpg" alt="">
                </div>
                <h3>Tina Krueger</h3>
                <h4>/ Lead Designer</h4>
                <p>Lead Designer mi porta gravida at eget metus id elit mi egetine...</p>
            </li>
            <li class="col-sm-3 col-xs-6">
                <div class="team-img">
                    <img class="img-responsive" src="owner/4.jpg" alt="">
                </div>
                <h3>David Case</h3>
                <h4>/ Web Developer</h4>
                <p>Web Developer in Unify agency porta gravida at eget metus id elit...</p>
            </li>
            <li class="col-sm-3 col-xs-6">
                <div class="team-img">
                    <img class="img-responsive" src="owner/1.jpg" alt="">

                </div>
                <h3>Kathy Reyes</h3>
                <h4>/ Creative Designer</h4>
                <p>Former Designer in Twitter non mi porta gravida at elit mi egetine...</p>
            </li>
        </ul>
    </div>-->

    <?php include('footer.php');?>
    <!--=== End Footer Version 1 ===-->
</div><!--/End Wrapepr-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<!--<script type="text/javascript" src="assets/plugins/parallax-slider/js/modernizr.js"></script>
<script type="text/javascript" src="assets/plugins/parallax-slider/js/jquery.cslider.js"></script>
<script type="text/javascript" src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>-->
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<!--<script type="text/javascript" src="assets/js/plugins/owl-carousel.js"></script>
<script type="text/javascript" src="assets/js/plugins/parallax-slider.js"></script>-->
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init();
        OwlCarousel.initOwlCarousel();
        ParallaxSlider.initParallaxSlider();
    });
</script>
<!--[if lt IE 9]>
<script src="assets/plugins/respond.js"></script>
<script src="assets/plugins/html5shiv.js"></script>
<script src="assets/plugins/placeholder-IE-fixes.js"></script>

<![endif]-->

</body>
</html>
