<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <title>Product</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="hlcp.png">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css'
          href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">
    <link rel="stylesheet" href="assets/css/theme-skins/dark.css">
    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">
</head>
<style>
    .lazy {
        width: 100%;
        height: 100%;
        display: block;
        background-image: url('test.gif');
        background-size: 50px 50px;
        background-repeat: no-repeat;
        background-position: 50% 50%;

    }
</style>
<body class="dark">

<div class="wrapper">
    <!--=== Header ===-->
    <?php include('header.php'); ?>


    <img  src="editedimg/bg2.jpg" class="lazy"width="100%" height="30%">

    <!--=== End Breadcrumbs v3 ===-->

    <!--=== Cube-Portfdlio ===-->
    <div class="cube-portfolio container margin-bottom-60" style="    background-color: #353535;padding-bottom: 3%;">
        <div class="content-xs">
            <div id="filters-container" class="cbp-l-filters-text content-xs">
                <h2 style="margin-bottom: -6px">Products</h2>
                <div data-filter="*" class="cbp-filter-item-active cbp-filter-item"> All</div>
                |
                <div data-filter=".primax" class="cbp-filter-item">Primax</div>
                |
                <div data-filter=".life" class="cbp-filter-item"> Life</div>
                |
                <div data-filter=".roadsafety" class="cbp-filter-item"> Road safety</div>
                |
                <div data-filter=".dht" class="cbp-filter-item "> Dye house trolly</div>
            </div><!--/end Filters Container-->
        </div>

        <div id="grid-container" class="cbp-l-grid-agency">
            <div class="cbp-item primax">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img  src="editedimg/p1.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <ul class="link-captions">

                                    <li><a href="editedimg/p1.jpg" class="cbp-lightbox"
                                           data-title="primax white 1200"><i
                                                    class="rounded-x icon-magnifier-add"></i></a></li>
                                </ul>
                                <div class="cbp-l-grid-agency-title">primax white 1200</div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div><!--/end Grid Container-->
    </div>


    <div class="footer-v1">

        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            H.L.C.P 2018 &copy; All Rights Reserved.
                            <a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>
                        </p>
                    </div>

                    <!-- Social Links -->
                    <div class="col-md-6">
                        <ul class="footer-socials list-inline">
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-src-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-src-title="Skype">
                                    <i class="fa fa-skype"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-src-title="Google Plus">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-src-title="Linkedin">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-src-title="Pinterest">
                                    <i class="fa fa-pinterest"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-src-title="Twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title=""
                                   data-src-title="Dribbble">
                                    <i class="fa fa-dribbble"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Social Links -->
                </div>
            </div>
        </div><!--/copyright-->
    </div>
    <!--=== End Footer Version 1 ===-->
</div><!--/End Wrapepr-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- JS Implementing Plugins -->

<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script type="text/javascript" src="assets/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/plugins/cube-portfolio/cube-portfolio-4.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ()
    {
        App.init();

        $.ajax({
            type: 'GET',
            url: 'includes/lazypictures.php',

            success: function(d)
            {
                jQuery("#grid-container").cubeportfolio('appendItems', d);

            }
        });
    });




</script>
<!--[if lt IE 9]>

<![endif]-->

</body>
</html>